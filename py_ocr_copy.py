# Run: python3 py_ocr_copy.py -i 006.jpg
# import the necessary packages
from PIL import Image
import pytesseract
import argparse
import cv2
import os
import numpy as np
from matplotlib import pyplot as plt 
#from count_dot import count
from read_coordinate import read_coordinate

 
# Xây dựng hệ thống tham số đầu vào
# -i file ảnh cần nhận dạng
# -p tham số tiền xử lý ảnh (có thể bỏ qua nếu không cần). Nếu dùng: blur : Làm mờ ảnh để giảm noise, thresh: Phân tách đen trắng
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="Đường dẫn đến ảnh muốn nhận dạng")
ap.add_argument("-p", "--preprocess", type=str, default="thresh",
	help="Bước tiền xử lý ảnh")
args = vars(ap.parse_args())

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
cnt,cropped = read_coordinate("006.txt","006.jpg")

#medianBlur = cv.medianBlur(dilation,3)
def open(img,kernel):
    medianBlur = cv2.medianBlur(thresh1,3)
    dilation = cv2.dilate(medianBlur,kernel,iterations = 1)
    erosion = cv2.erode(dilation,kernel, iterations = 1)
    return erosion

def close(img,kernel):
    erosion = cv2.erode(img,kernel, iterations = 1)
    medianBlur = cv2.medianBlur(erosion,3)
    dilation = cv2.dilate(medianBlur,kernel,iterations = 1)

    return dilation

def count(image):
    img = image.copy()
    ## threshold
    th, threshed = cv2.threshold(image, 127, 255,cv2.THRESH_BINARY)
    ## findcontours
    cnts = cv2.findContours(threshed, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2]
    ## filter by area
    s1= 3
    s2 = 90
    xcnts = []
    frame = np.zeros(image.shape)
    for i,cnt in enumerate(cnts):
        if s1<cv2.contourArea(cnt) <s2:
            xcnts.append(cnt)
            cv2.drawContours(frame, cnts, i, 255,10 )
    image[frame==0] = 255
    img[image==0] = 255
    m = cv2.medianBlur(img,3)
    th, threshed2 = cv2.threshold(m, 100, 255,cv2.THRESH_BINARY)
    m2 = open(m,kernel)
    m3 = close(m,kernel)
    #cv2.imshow("m2",threshed2)
    '''
    print("Dots number: {}".format(len(xcnts)))
    cv2.imshow("image",threshed2)
    cv2.imshow("m2",m2)
    cv2.imshow("m3",m3)
    '''
    return threshed2 #threshed2, m3

for i in range(cnt):
    img = cv2.imread("crop{}.png".format(i),0)
    image = img.copy()
    medianBlur = cv2.medianBlur(img,3)
    ret,thresh1 = cv2.threshold(img, 127,255,cv2.THRESH_BINARY)
    kernel = np.ones((3,3),np.uint8)
    erosion = cv2.erode(thresh1,kernel, iterations = 1)
    dilation = cv2.dilate(thresh1,kernel,iterations = 1)

    edges = cv2.Canny(img,50,150,apertureSize = 3)
    minLineLength = 100
    maxLineGap = 10
    
    opening = open(thresh1,kernel)
    closing = close(opening,kernel)
    #medianBlur = cv2.medianBlur(img,13)
    gauss = cv2.GaussianBlur(img, (3,3), 0)
    subtractDot = count(img)
    #m = cv2.medianBlur(subtractDot,7)
    titles = ["original","erosion","dilation","opening","closing","medianBlur","gaussianBlur","thresh1","subtractDot"]
    images = [image,erosion,dilation,opening,closing,medianBlur,gauss,thresh1,subtractDot]
    
    for i in range(9):
        plt.subplot(3,3,i+1),plt.imshow(images[i],"gray")
        plt.title(titles[i]) 
    plt.show()
    
    
    # Ghi tạm ảnh xuống ổ cứng để sau đó apply OCR
    filename1 = "{}.png".format(os.getpid())

    cv2.imwrite(filename1,subtractDot)

    # Load ảnh và apply nhận dạng bằng Tesseract OCR
    text1 = pytesseract.image_to_string(Image.open(filename1),lang='vie')

    # Xóa ảnh tạm sau khi nhận dạng
    os.remove(filename1)

    # In dòng chữ nhận dạng được
    print(text1)
    
    # Hiển thị các ảnh chúng ta đã xử lý.
    #cv2.imshow("Image", image)
    #cv2.imshow("Output", gray)

    # Đợi chúng ta gõ phím bất kỳ
cv2.waitKey(0)
