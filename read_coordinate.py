import cv2
import numpy as np 
from matplotlib import pyplot as plt
from PIL import Image
import csv

def read_coordinate(file_name,image_name):
    def resize_image(img):
        img_size = img.shape
        im_size_min = np.min(img_size[0:2])
        im_size_max = np.max(img_size[0:2])

        im_scale = float(600) / float(im_size_min)
        if np.round(im_scale * im_size_max) > 1200:
            im_scale = float(1200) / float(im_size_max)
        new_h = int(img_size[0] * im_scale)
        new_w = int(img_size[1] * im_scale)

        new_h = new_h if new_h // 16 == 0 else (new_h // 16 + 1) * 16
        new_w = new_w if new_w // 16 == 0 else (new_w // 16 + 1) * 16

        re_im = cv2.resize(img, (new_w, new_h), interpolation=cv2.INTER_LINEAR)
        return re_im, (new_h / img_size[0], new_w / img_size[1])


    file = open(file_name,"r")


    # count the rows in file
    def file_len(fname):
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1
    cnt = file_len(file_name)
    #print("cnt = ",cnt)

    x1,y1,x2,y2,x3,y3,x4,y4 = [], [],[],[],[],[],[],[]
    #print each row in file
    for i in range(cnt):
        #line = file.readline()
        reader = csv.reader(file,delimiter=',')
        # print(reader)
        for row in reader:
            #print(row)
            x1.append(np.int(row[2]))
            y1.append(np.int(row[3]))
            x2.append(np.int(row[4]))
            y2.append(np.int(row[5]))
            x3.append(np.int(row[6]))
            y3.append(np.int(row[7]))
            x4.append(np.int(row[0]))
            y4.append(np.int(row[1]))
            
    '''        
    print("x1 = ",x1)
    print("y1 = ",y1)
    print("x2 = ",x2)
    print("y2 = ",y2)
    print("x3 = ",x3)
    print("y3 = ",y3)
    print("x4 = ",x4)
    print("y4 = ",y4)
    '''
    def sortSecond(val): 
        return val[1]  
    y4_copy = y4.copy()
    y4.sort()
    x = []
    #print("y4_copy ",y4_copy)
    #print("y4_sort ",y4)
    for val in y4:
            if (y4[y4.index(val)]==val):
                x.append(y4_copy.index(val))
                #print(y4_copy.index(val))
    #print("x ",x)
                
    #new_img1 = cv2.rectangle(fixed_image, (x3,y3), (x1,y1), (0, 255, 0), 5)
    for j in range(cnt):
        a = int(y1[x[j]])
        #print("a", a)
        image_origin = cv2.imread(image_name)
        image, (rh,rw) = resize_image(image_origin)
        #fixed_image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        #im = Image.open("/home/thanhhang/Documents/ocr-copy/ocr/006.jpg") 
        cropped = image[int(y1[x[j]]):int(y3[x[j]]),int(x3[x[j]]):int(x1[x[j]])]
        cv2.imwrite("crop{}.png".format(j),cropped)

    #cv2.imshow("new image",new_img1)

    cv2.waitKey(0)
    return cnt,cropped