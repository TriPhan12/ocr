import cv2
import imutils
import numpy as np

class ID_Detection(object):
    def __init__(self, src_image):
        self.src_img = src_image

    def detect_average_color(self):
        # Detect average color of the whole image
        img_height = self.src_img.shape[0]
        img_width = self.src_img.shape[1]
        if img_height < img_width:
            img = imutils.resize(self.src_img, width=min(20, img_width))
        else:
            img = imutils.resize(self.src_img,height=min(40, img_height))
        avg_color = img.mean(axis=0).mean(axis=0)
        print(avg_color)
        return avg_color

class ID_Morphological(object):

    def __init__(self, input_image):
        self.img = input_image

    def erosion(self, in_img, kernel_size = 3, iterate_num = 1):
        kernel = np.ones((kernel_size,kernel_size), np.uint8)
        if in_img is None:
            in_img = self.img
        erosion = cv2.erode(in_img, kernel, iterations = iterate_num)
        return erosion  

    def dilation(self, in_img, kernel_size = 3, iterate_num = 1):
        kernel = np.ones((kernel_size//3,kernel_size*8000), np.uint8)
        if in_img is None:
            in_img = self.img
        dilation = cv2.dilate(in_img, kernel, iterations = iterate_num)
        return dilation

    def opening(self, kernel_size=6):
        kernel = np.ones((kernel_size,kernel_size), np.uint8)
        opening = cv2.morphologyEx(self.img, cv2.MORPH_OPEN, kernel)
        return opening

    def closing(self, kernel_size=5):
        kernel = np.ones((kernel_size,kernel_size), np.uint8)
        closing = cv2.morphologyEx(self.img, cv2.MORPH_CLOSE, kernel)
        return closing

    def ero_dilate(self, erode_num = 1, dilate_num = 1):
        
        eroded_image = self.dilation(in_img=None, iterate_num= erode_num)
        dilated_image = self.erosion(in_img= eroded_image, iterate_num= dilate_num)
        return dilated_image


class ID_Thresholding(object):
    def __init__(self, input_image):
        self.src_img = input_image

    def thresholding(self):
        # Convet image into gray scale
        if len(self.src_img.shape) == 3:
            gray_img = cv2.cvtColor(self.src_img, cv2.COLOR_BGR2GRAY)
        elif len(self.src_img.shape) == 2:
            gray_img = self.src_img
        thresh_img = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        return thresh_img

class detect_Line(object):
    def __init__(self, src_img):
        self.src_img = src_img

    def detect(self):
        if len(self.src_img.shape) == 2:    # gray image
            gray_img = self.src_img
        elif len(self.src_img.shape) ==3:   # RGB image
            gray_img = cv2.cvtColor(self.src_img, cv2.COLOR_BGR2GRAY)

        thresh_img = cv2.adaptiveThreshold(~gray_img,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,-2)
        h_img = thresh_img.copy()
        v_img = thresh_img.copy()
        scale = 15
        h_size = int(h_img.shape[1]/scale)

        h_structure = cv2.getStructuringElement(cv2.MORPH_RECT,(h_size,1)) # Morphological factor
        h_erode_img = cv2.erode(h_img,h_structure,1)

        h_dilate_img = cv2.dilate(h_erode_img,h_structure,1)
        cv2.imshow("h_erode",h_dilate_img)
        v_size = int(v_img.shape[0] / scale)

        v_structure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, v_size//2))  # Morphological factor
        v_erode_img = cv2.erode(v_img, v_structure, 1)
        v_dilate_img = cv2.dilate(v_erode_img, v_structure, 1)
        cv2.imshow("v_erode",v_dilate_img)


        mask_img = h_dilate_img+v_dilate_img
        joints_img = cv2.bitwise_and(h_dilate_img,v_dilate_img)
        cv2.imshow("joints",joints_img)
        cv2.imshow("mask",mask_img)
        # cv2.imwrite("images/mask.jpg", mask_img)
        # return mask_img,joints_img


