#  Run: python3 py_ocr.py -i 006.jpg
# import the necessary packages
from PIL import Image
import pytesseract
import argparse
import cv2
import os
import numpy as np
from matplotlib import pyplot as plt 
import imutils
 
# Xây dựng hệ thống tham số đầu vào
# -i file ảnh cần nhận dạng
# -p tham số tiền xử lý ảnh (có thể bỏ qua nếu không cần). Nếu dùng: blur : Làm mờ ảnh để giảm noise, thresh: Phân tách đen trắng
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="Đường dẫn đến ảnh muốn nhận dạng")
ap.add_argument("-p", "--preprocess", type=str, default="thresh",
	help="Bước tiền xử lý ảnh")
args = vars(ap.parse_args())

# Using multi morphology method for image processing 
def multi_morphology(input_image):
    #print("This is multi morphology fun.")
    original = input_image.copy()
    ret,thresh1 = cv2.threshold(original, 127,255,cv2.THRESH_BINARY)
    kernel = np.ones((3,3),np.uint8)
    erosion = cv2.erode(thresh1,kernel, iterations = 2)
    dilation = cv2.dilate(thresh1,kernel,iterations = 1)

    opening = open(thresh1,kernel)
    closing = close(opening,kernel)
    medianBlur = cv2.medianBlur(thresh1,5)
    gauss = cv2.GaussianBlur(original, (5,5), 0)
    
    edges = cv2.Canny(original,50,150,apertureSize = 3)
    minLineLength = 100
    maxLineGap = 10
    lines = cv2.HoughLinesP(edges,1,np.pi/180,100,minLineLength,maxLineGap)
    for line in lines:
        for x1,y1,x2,y2 in line:
            cv2.line(original,(x1,y1),(x2,y2),(0,0,255),2)
    # cv2.imshow("houghlines", original)
    open_img,close_img,thresh_img = count(input_image)
    
    cv2.destroyAllWindows()
    '''
    titles = ["original","erosion","dilation","opening","closing","medianBlur","gaussianBlur","thresh1","subtracDot1","subtracDot2","subtracDot3"]
    images = [original,erosion,dilation,opening,closing,medianBlur,gauss,thresh1,open_img,close_img,thresh_img]
    
    for i in range(11):
        plt.subplot(4,3,i+1),plt.imshow(images[i],"gray")
        plt.title(titles[i]) 
    plt.show()
    cv2.waitKey(0)
    '''
    # medianBlur = cv2.medianBlur(dilation,3)
    return erosion,dilation,opening,closing,medianBlur,gauss,thresh1,open_img,close_img,thresh_img

# count dot function
def count(image):
    img = image.copy()
    
    # Declare kernel
    #kernel = np.diamon((3,3),np.uint8)

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(2,2))
    ## threshold
    th, threshed = cv2.threshold(image, 127, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    ## findcontours
    cnts = cv2.findContours(threshed, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2]
    ## filter by area
    s1= 3
    s2 = 90
    xcnts = []
    frame = np.zeros(image.shape)
    for i,cnt in enumerate(cnts):
        if s1<cv2.contourArea(cnt) <s2:
            xcnts.append(cnt)
            cv2.drawContours(frame, cnts, i, 255,10 )
    image[frame==0] = 255
    img[image==0] = 255
    m = cv2.medianBlur(img,5)
    th, threshed2 = cv2.threshold(m, 100, 255,cv2.THRESH_BINARY)
    open_img = open(m,kernel)
    close_img = close(m,kernel)

    print("Dots number: {}".format(len(xcnts)))
    return open_img,close_img,threshed2

def open(img,kernel):
    # medianBlur = cv2.medianBlur(img,3)
    dilation = cv2.dilate(img,kernel,iterations = 1)
    erosion = cv2.erode(dilation,kernel, iterations = 1)
    return erosion

def close(img,kernel):
    erosion = cv2.erode(img,kernel, iterations = 1)
    # medianBlur = cv2.medianBlur(erosion,3)
    dilation = cv2.dilate(erosion,kernel,iterations = 1)
    return dilation

def ocr(input_image):
    # Chuyển về ảnh xám
    # Convert image to gray scale
    # print("This is ocr fun.")

    if len(input_image.shape) == 2:
        gray = input_image
    elif len(input_image.shape) == 3:
        gray = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
    
    # erosion,dilation,opening,closing,medianBlur,gauss,thresh1,open_img, close_img, thresh_new = multi_morphology(gray)
    open_img = multi_morphology(gray)[9]
    
    # Check xem có sử dụng tiền xử lý ảnh không
    # Nếu phân tách đen trắng
    if args["preprocess"] == "thresh":
        gray = cv2.threshold(gray, 0, 255,
            # cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
            cv2.THRESH_OTSU)[1]
    
    # Nếu làm mờ ảnh
    elif args["preprocess"] == "blur":
        gray = cv2.medianBlur(gray, 3)
        # cv2.imwrite("blur.png", gray)

    # Ghi tạm ảnh xuống ổ cứng để sau đó apply OCR
    filename1 = "{}.png".format(os.getpid())

    cv2.imwrite(filename1,open_img)
    # Load ảnh và apply nhận dạng bằng Tesseract OCR
    text = pytesseract.image_to_string(Image.open(filename1),lang='vie')

    # Xóa ảnh tạm sau khi nhận dạng
    os.remove(filename1)

    # In dòng chữ nhận dạng được
    print(text)
    
    # Hiển thị các ảnh chúng ta đã xử lý.
    cv2.imshow("Image", input_image)
    cv2.imshow("Output",open_img)

    # Đợi chúng ta gõ phím bất kỳ
    cv2.waitKey(0)

def main():
    # Import and resize input image
    source_image = cv2.imread(args["image"])
    image_height = source_image.shape[0]
    image_width = source_image.shape[1]
    if image_height < image_width:
        input_image = imutils.resize(source_image, width= min(1200, image_width))
    else:
        input_image = imutils.resize(source_image,height= min(700, image_height))
    
    

    # Funtions
    # multi_morphology(input_image)
    ocr(input_image)
    
if __name__ == "__main__":
    main()
