import cv2
from preprocessing import ID_Morphological, ID_Thresholding, detect_Line, ID_Detection
import imutils
import time
# Load the input image
img = cv2.imread("images/Phap/Phap2.jpg")
img_height = img.shape[0]
img_width = img.shape[1]
if img_height < img_width:
    img = imutils.resize(img, width=min(900, img_width))
else:
    img = imutils.resize(img,height=min(600, img_height))

# Define object
threshold = ID_Thresholding(img)
pre = ID_Morphological(img)
line_detector = detect_Line(img)
color = ID_Detection(img)
# Show the origin image
cv2.imshow("Origin", img)

# #Show the OpenCV opening image

# opening = pre.opening(kernel_size= 5)
# cv2.imshow("opening", opening)

#Show the OpenCV closing image

closing = pre.closing(kernel_size= 3)
cv2.imshow("closing", closing)
cv2.imwrite("closing.jpg", closing)
#Show erotion and dilation image 
ero = pre.dilation(in_img= None)
cv2.imshow("erotion", ero)
dila = pre.erosion(in_img= None)
cv2.imshow("dilation", dila)

#Show the designed algorithm image
algo = pre.ero_dilate()
cv2.imshow("algo", algo)

# #Thresholding the image
thres = threshold.thresholding()
cv2.imshow("Thresholded image", thres)

# detect line in image
line_detector.detect()

#Show average color of the whole image
start = time.time()
color_detect = tuple(color.detect_average_color())

print("consumtion: ", time.time()-start)
cv2.putText(img, "This is the color", (0,30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, color_detect, thickness=1, lineType=2)
cv2.imshow("output", img)
# Wait for any pressed key
cv2.waitKey(0)