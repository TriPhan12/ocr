import cv2
import numpy as np 

#kernel = np.diamon((3,3),np.uint8)

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
def open(img,kernel):
    #medianBlur = cv2.medianBlur(img,3)
    dilation = cv2.dilate(img,kernel,iterations = 1)
    erosion = cv2.erode(dilation,kernel, iterations = 1)
    return erosion


def close(img,kernel):
    erosion = cv2.erode(img,kernel, iterations = 1)
    #medianBlur = cv2.medianBlur(erosion,1)
    dilation = cv2.dilate(erosion,kernel,iterations = 1)
    return dilation

def count(image):
    img = image.copy()
    ## threshold
    th, threshed = cv2.threshold(image, 80, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    ## findcontours
    cnts = cv2.findContours(threshed, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2]
    ## filter by area
    s1= 3
    s2 = 90
    xcnts = []
    frame = np.zeros(image.shape)
    for i,cnt in enumerate(cnts):
        if s1<cv2.contourArea(cnt) <s2:
            xcnts.append(cnt)
            cv2.drawContours(frame, cnts, i, 255,10 )
    image[frame==0] = 255
    img[image==0] = 255
    m = cv2.medianBlur(img,7)
    th, threshed2 = cv2.threshold(m, 60, 255,cv2.THRESH_BINARY)
    m2 = open(m,kernel)
    m3 = close(m,kernel)


    print("Dots number: {}".format(len(xcnts)))
    '''
    cv2.imshow("image",threshed2)
    cv2.imshow("m2",m2)
    cv2.imshow("m3",m3)
    '''

    return m2,m3,threshed2
'''
image = cv2.imread("test.png",0)
img = count(image)
cv2.waitKey(0)
'''
