import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt 



img = cv.imread("test.png")
ret,thresh1 = cv.threshold(img, 30,255,cv.THRESH_BINARY)
ret,thresh2 = cv.threshold(img, 100,255,cv.THRESH_BINARY)
ret,thresh3 = cv.threshold(img, 150,255,cv.THRESH_BINARY)
ret,thresh4 = cv.threshold(img, 200,255,cv.THRESH_BINARY)
kernel = np.ones((3,3),np.uint8)
erosion = cv.erode(thresh1,kernel, iterations = 2)
dilation = cv.dilate(thresh1,kernel,iterations = 1)

edges = cv.Canny(img,50,150,apertureSize = 3)
minLineLength = 100
maxLineGap = 10
lines = cv.HoughLinesP(edges,1,np.pi/180,100,minLineLength,maxLineGap)
for x1,y1,x2,y2 in lines[0]:
    cv.line(img,(x1,y1),(x2,y2),(0,0,255),2)

cv.imwrite('houghlines3.jpg',img)
line = cv.imread("houghlines3.jpg")
#medianBlur = cv.medianBlur(dilation,3)
def open(img,kernel):
    medianBlur = cv.medianBlur(thresh1,5)
    dilation = cv.dilate(medianBlur,kernel,iterations = 2)
    erosion = cv.erode(dilation,kernel, iterations = 2)
    return erosion

def close(img,kernel):
    erosion = cv.erode(img,kernel, iterations = 1)
    medianBlur = cv.medianBlur(erosion,3)
    dilation = cv.dilate(medianBlur,kernel,iterations = 1)

    return dilation

opening = open(thresh1,kernel)
closing = close(opening,kernel)
medianBlur = cv.medianBlur(thresh1,5)
gauss = cv.GaussianBlur(img, (5,5), 0)
ret,thresh5 = cv.threshold(gauss,100,255,cv.THRESH_BINARY)

titles = ["original","erosion","dilation","opening","closing","medianBlur","gaussianBlur","thresh1","thresh2","thresh3","thresh5","line"]
images = [img,erosion,dilation,opening,closing,medianBlur,gauss,thresh1,thresh2,thresh3,thresh5,line]
for i in range(12):
    plt.subplot(4,3,i+1),plt.imshow(images[i],"gray")
    plt.title(titles[i]) 
plt.show()
cv.waitKey(0)
cv.destroyAllWindows()
