import cv2
import imutils
import time
# Load the input image
img = cv2.imread("images/Cong/Cong.jpg")
img_height = img.shape[0]
img_width = img.shape[1]
if img_height < img_width:
    img = imutils.resize(img, width=min(200, img_width))
else:
    img = imutils.resize(img,height=min(400, img_height))

average = img.mean(axis=0).mean(axis=0)

# Show the origin image
cv2.imshow("Origin", img)

cv2.waitKey(0)
start = time.time()
average = img.mean(axis=0).mean(axis=0)
print(average)
print("consumtion: ", time.time()-start)

import numpy as np
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
pixels = np.float32(img.reshape(-1, 3))

n_colors = 10
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
flags = cv2.KMEANS_RANDOM_CENTERS

_, labels, palette = cv2.kmeans(pixels, n_colors, None, criteria, 10, flags)
_, counts = np.unique(labels, return_counts=True)
dominant = palette[np.argmax(counts)]
print("dominant", dominant)
import matplotlib.pyplot as plt
avg_patch = np.ones(shape=img.shape, dtype=np.uint8)*np.uint8(average)

indices = np.argsort(counts)[::-1]   
freqs = np.cumsum(np.hstack([[0], counts[indices]/counts.sum()]))
rows = np.int_(img.shape[0]*freqs)

dom_patch = np.zeros(shape=img.shape, dtype=np.uint8)
for i in range(len(rows) - 1):
    dom_patch[rows[i]:rows[i + 1], :, :] += np.uint8(palette[indices[i]])

fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(12,6))
ax0.imshow(avg_patch)
ax0.set_title('Average color')
ax0.axis('off')
ax1.imshow(dom_patch)
ax1.set_title('Dominant colors')
ax1.axis('off')
plt.show(fig)